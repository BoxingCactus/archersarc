﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    public void StartGame()
    {
        SceneManager.LoadScene("MainScene");
    }
    public void EndGame()
    {
        Application.Quit();
    }
    public void RollCredits()
    {
        SceneManager.LoadScene("Credits");
    }
    public void BacktoStart()
    {
        SceneManager.LoadScene("TitleScreen");
    }
    public void ToLore()
    {
        SceneManager.LoadScene("Lore");
    }
    public void ReadControls()
    {
        SceneManager.LoadScene("Controls");
    }

}
