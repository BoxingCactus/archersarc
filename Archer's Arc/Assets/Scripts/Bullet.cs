﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public AudioSource sound;
    void Start()
    {
        
    }

    // Update is called once per frame
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Wall")) 
        {
            Destroy(gameObject);
        }
        if (other.gameObject.CompareTag("Player")) 
        {
            Destroy(gameObject);
        }

        if (other.gameObject.CompareTag("Target"))
        {
            other.gameObject.SetActive(false);
            Destroy(gameObject);
            sound.Play();
        }
        if (other.gameObject.CompareTag("Bullet")) 
        {
            Destroy(gameObject);
        }
        if (other.gameObject.CompareTag("Fence")) 
        {
            Physics2D.IgnoreCollision(other.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }
    }
}
