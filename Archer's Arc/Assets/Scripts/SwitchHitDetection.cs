﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class SwitchHitDetection : MonoBehaviour
{
    public AudioSource sound;
    private float Hits = 1;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Bullet") && Hits != 0) 
        {
        sound.Play();
        Hits = 0;
        }
    }
}
